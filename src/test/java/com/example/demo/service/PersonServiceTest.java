package com.example.demo.service;

import com.example.demo.model.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class PersonServiceTest {
    private final PersonService personService;

    @Autowired
    public PersonServiceTest(PersonService personService){this.personService = personService;}

    @Test
    void insertPerson() {

        assertEquals(1, personService.insertPerson(new Person(null, "test name")));

    }
}